import Vue from 'vue';
import Moment from 'vue-moment';
import Vuex from 'vuex';
import App from './App.vue';
import Vuelidate from 'vuelidate';
import store from './store/index';
import 'bootstrap';
import './assets/icofont/icofont.min.css';
import './assets/sass/variable.scss';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'animate.css';

Vue.config.productionTip = false
Vue.use(Moment);
Vue.use(Vuex);
Vue.use(Vuelidate);

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
