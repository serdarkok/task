const state =  () => ({
    participant : null,
});

const actions = {
    async addParticipant({commit}, data){
        await commit('ADD_PARTICIPANT', data);
    },

    async addWorkshops({commit}, data) {
        await commit('ADD_WORKSHOPS', data);
    },

    async removeParticipant({commit}) {
        await commit('REMOVE_PARTICIPANT');
    } 
}

const mutations = {
    ADD_PARTICIPANT(state, data) {
        state.participant = data;
        localStorage.setItem('participant', JSON.stringify(state.participant));
    },

    ADD_WORKSHOPS(state, data) {
        state.participant = {...state.participant, workshops: data};
        localStorage.setItem('participant', JSON.stringify(state.participant));
    },

    REMOVE_PARTICIPANT(state) {
        state.participant = null;
        localStorage.removeItem('participant');
    }
}

const getters = {
    getParticipants(state) {
        return state.participant;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
}