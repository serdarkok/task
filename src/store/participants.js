const state =  () => ({
    participants : {
        registrations: [],
    },
});

const actions = {
    // get participant from client then push them to the state and localStorage
    async addParticipant({commit}, data){
        await commit('ADD_PARTICIPANT', data);
    },

    // if client is refreshing the page, get All Participants from localStorage then put them to the state
    async putParticipant({commit}, data) {
        await commit('PUT_PARTICIPANTS', data);
    },

    async removeParticipant({commit}) {
        await commit('REMOVE_PARTICIPANT');
    } 
}

const mutations = {
    ADD_PARTICIPANT(state, data) {
        state.participants.registrations.push(data);
        localStorage.setItem('participants', JSON.stringify(state.participants));
    },

    PUT_PARTICIPANTS(state, data) {
        state.participants.registrations = data;
    },

    REMOVE_PARTICIPANT(state) {
        state.participants = null;
        localStorage.removeItem('participants');
    }
}

const getters = {
    getParticipants(state) {
        return state.participants;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
}