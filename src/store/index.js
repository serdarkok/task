import Event from './event';
import Level from './level';
import Vuex from 'vuex';
// import Participant from './participant';
import Participant from './currentParticipant';
import Participants from './participants';

const createStore = () => {
    return new Vuex.Store({
        modules: {
            event: Event,
            level: Level,
            participant: Participant,
            participants: Participants,
        },
    });
}

export default createStore;
