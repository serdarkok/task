// Forms Step Level

const state = () => ({
    level: 1,
});

const actions = {
    
    async increaseLevel({commit}) {
        commit('INCREASE_LEVEL');
    },

    async decreaseLevel({commit}) {
        commit('DECREASE_LEVEL');
    },

    async putLevel({commit}, data) {
        commit('PUT_LEVEL', data);
    }
}

const mutations = {
    INCREASE_LEVEL(state) {
        state.level++;
        localStorage.setItem('level', JSON.stringify(state.level));
    },
    DECREASE_LEVEL(state) {
        state.level--;
        localStorage.setItem('level', JSON.stringify(state.level));
    },
    PUT_LEVEL(state, data) {
        state.level = data;
        localStorage.setItem('level', JSON.stringify(state.level));
    }
}

const getters = {
    getLevel(state) {
        return state.level;
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
}

