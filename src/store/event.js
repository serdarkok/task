const state = () => ({
    event: {},
});

const actions = {
    async addEvent({commit}, data) {
        commit('ADD_EVENT', data);
    }
}

const mutations = {
    ADD_EVENT(state, data) {
        state.event = data;
        localStorage.setItem('event', JSON.stringify(state.event));
    }
}

const getters = {
    getWorkshops(state) {
        return state.event.workshops;
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
    getters,
}
